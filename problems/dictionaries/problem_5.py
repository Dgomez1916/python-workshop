# Complete the function horizontal_bar_chart 
# below that takes in a string and creates a 
# horizontal bar chart made out of the instances 
# of the letters in that string based on the 
# number of each letter.

# The return value is a list of the strings of 
# the letters in alphabetical order.

# For example, given the sentence "abba has a banana", 
# it would return this list of strings because there 
# are seven "a", three "b", one "h", two "n", and one "s" 
# letters in it:

[
  "aaaaaaa",
  "bbb",
  "h",
  "nn",
  "s",
]

# All letters in the input will be lowercase letters.
# You may want to look at the methods for dictionaries 
# and the built-in functions.
# Remember, you can also compare strings with >= and <=.

def horizontal_bar_chart(sentence):
    pass