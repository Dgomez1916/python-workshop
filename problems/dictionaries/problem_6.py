# Suppose you have a list of player records where 
# each player's record is a dictionary. The dictionary 
# contains the player's name, score, and whether they 
# are still in the game or not. The task is to write 
# a function that will find the names of the players 
# who are still in the game and have a score over a 
# certain threshold. Some of the player record data 
# that indicates whether or not they are in the game 
# may be missing, be sure to account for that in your 
# solution.

game_records = [
    {"name": "Alice", "score": 120},
    {"name": "Bob", "score": 80, "in_game": True},
    {"name": "Charlie", "score": 200, "in_game": False},
    {"name": "Dave", "score": 90},
    {"name": "Eve", "score": 130, "in_game": True},
]

# Example:
# result = high_scoring_players(game_records, 100)
# print(result) # prints ['Eve']

def high_scoring_players(records, score_threshold):
	pass