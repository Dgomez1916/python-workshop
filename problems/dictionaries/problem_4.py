# Create a function that takes a list of grades
# (integers) and counts how many students got each grade.
# It then prints the result (ordered by the grade = extra)

# The below code is an example method of creating 
# a random sample of data which it assigns to
# a list called "sample_grades"
import random
# for ease of use, we're saying possible grades can
# only be 10's 
possible_grades = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
sample_grades = random.choices(
    possible_grades, 
    # these weights are arbitrary
    weights=(1,2,4,6,8,12,20,15,8,1), 
    # number of "students"
    k=500
)


