# We want to ensure 2 values divide evenly.
# Write a function taking two values: price 
# and quantity. All values will be greater 
# than 0. If the result of the price 
# divided by the quantity results in a number 
# that has values other than 0 after the decimal 
# point, then this function should return True. 
# Otherwise, it returns False.

# If the price or quantity is zero, 
# it should return None.

# Examples:

#   price	quantity output
#   6	    1.5	     False
#   8	    2	     False
#   6	    3	     False
#   6	    4	     True
#   6	    0	     None
#   0	    6	     None

# You can test if a number is an integer by using the
# int function: if int(x) == x: