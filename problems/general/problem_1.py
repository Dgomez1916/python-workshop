# There's a "3 for 2" offer on apples. 
# *** The 2 means 2 dollars/euros/pesos/whatev ***  
# For a given quantity and price (per 3 apples),
# calculate the total cost of the apples, 
# plus tax. Apples can only be purchased in 
# quantities of 3.

# Implement the function calculate_total_with_tax, 
# which will return the cost of the apples with 
# the tax included.

# For example, if the amount of apples is 3 and 
# the tax rate is 0.1, then the tax is 2 * 0.1 = .20. 
# We add the total and the tax together to get the total 2 + .2 = 2.20.

# total 	tax_rate    output
# 6	        0.3	        5.20
# 9 	    0.25	    7.5
# 12	    0.25	    10

