# Write a function string_tokens that will
# take a string that's composed of values that
# are separated by some "delimiter" character,
# like a comma. It then returns the number of
# values in that string that are separated by
# the provided delimiter.

#Here are some example inputs and outputs:

# line	    separator	output
# ""	        any	        0
# "a"	        ""	        1
# ","	        ","	        2
# "a,b,c"	    ","	        3
# "a#b#c"	    "#"	        3

def string_tokens(line, separator):
    pass