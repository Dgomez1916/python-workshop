
class Vehicle:
    def __init__(self, name, max_speed, mileage):
        self.name = name
        self.max_speed = max_speed
        self.mileage = mileage

class Bus(Vehicle):
    def __init__(self, name, max_speed, mileage, capacity):
        super().__init__(name, max_speed, mileage)
        self.capacity = capacity

    def fare(self):
        price = self.max_speed / self.capacity * 1.00
        return round(price, 2)
    
bus = Bus("Superbus", 95, 132000, 80)
print(f"This {bus.name} has {bus.mileage} miles on it.",
    f"It can travel up to {bus.max_speed} MPH.",
    f"A single fare costs ${bus.fare()}"
)