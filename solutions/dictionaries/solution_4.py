
import random
possible_grades = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
sample_grades = random.choices(
    possible_grades, 
    weights=(1,2,4,6,8,12,20,15,8,1), 
    k=100
)


def count_each_grade(grades):
    # --- First use a dictionary to organize and
    # track the data
    grade_tracker = {}
    for grade in grades:
        if grade in grade_tracker:
            grade_tracker[grade] += 1
        else:
            grade_tracker[grade] = 1
    
    # --- Then sort the data by grade (key)
    # This part can be given away as an example,
    # the above is the important part
    sorted_list = sorted(grade_tracker.items())
    for item in sorted_list:
        print(f"{item[0]}: {item[1]}")

count_each_grade(sample_grades)