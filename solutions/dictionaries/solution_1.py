
keys = ['Ten', 'Twenty', 'Thirty']
values = [10, 20, 30]


def convert_to_dict(key_list, value_list):
    # optional validation
    if len(key_list) != len(value_list):
        print("Lists are not the same length!")
        return
        # or...
        # raise ValueError("Lists are not the same length!")

    new_dict = dict()
    for i in range(len(key_list)):
        new_dict[key_list[i]] = value_list[i]
    return new_dict

result = convert_to_dict(keys, values)
print(result)

