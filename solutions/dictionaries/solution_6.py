
game_records = [
    {"name": "Alice", "score": 120},
    {"name": "Bob", "score": 80, "in_game": True},
    {"name": "Charlie", "score": 200, "in_game": False},
    {"name": "Dave", "score": 90},
    {"name": "Eve", "score": 130, "in_game": True},
]

def high_scoring_players(records, score_threshold):
    players = []
    for record in records:
        if record.get("in_game") and record.get("score") > score_threshold:
            players.append(record.get("name"))
    return players


result = high_scoring_players(game_records, 100)
print(result)