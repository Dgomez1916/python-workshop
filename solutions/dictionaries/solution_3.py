
sample_dict = {
    "name": "Kelly",
    "age": 25,
    "salary": 8000,
    "city": "New york"}

# Keys to extract
keys = ["name", "salary"]


def extract_keys(key_list, dict):
    new_dict = {}
    for key in key_list:
        if key in dict:
            new_dict[key] = dict[key]
    return new_dict


result = extract_keys(keys, sample_dict)
print(result)