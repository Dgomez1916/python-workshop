
def string_tokens(line, separator):
    if separator == "":
        return 0
    else:
        return len(line.split(separator))

string = ""
sep = "foobar"
print(string_tokens(string, sep))
string = "a,b,c"
sep = ""
print(string_tokens(string, sep))
string = "a,b,c"
sep = ","
print(string_tokens(string, sep))
string = "a#b#c"
sep = "#"
print(string_tokens(string, sep))
string = "a,bc,d,,f"
sep = ","
print(string_tokens(string, sep))