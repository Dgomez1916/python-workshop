
def calculate_total_with_tax(apples, tax_rate):
    total_apple_cost = (apples / 3) * 2
    tax = total_apple_cost * tax_rate
    return total_apple_cost + tax

result = calculate_total_with_tax(9, .25)
print(result)
